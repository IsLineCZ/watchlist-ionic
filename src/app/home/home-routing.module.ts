import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
  },
  {
    path: 'show-detail',
    loadChildren: () => import('./show-detail/show-detail.module').then( m => m.ShowDetailPageModule)
  },
  {
    path: 'favourite-show-list',
    loadChildren: () => import('./favourite-show-list/favourite-show-list.module').then( m => m.FavouriteShowListPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
