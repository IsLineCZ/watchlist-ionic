import { Component, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { ShowPreview } from '../models/show-preview.model';
import { ApiService } from '../services/api.service';
import { NotificationService } from '../services/notification.service';
import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
	@ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
	shows: ShowPreview[];
	currentPage: number = 1;
	showSearchbar: boolean;
	searchBarValue: string = "";

  constructor(
		private apiService: ApiService,
		private storageService: StorageService,
		private notificationService: NotificationService
	) {}

	ngOnInit() {}

	ionViewDidEnter() {
		this.getShows();
	}

	getShows() {
		this.apiService.getShows().then((shows) => this.shows = shows);
	}

	loadShows(event) {
		this.currentPage++;
		if (this.showSearchbar && this.searchBarValue !== "") {
			this.apiService.searchShows(this.currentPage, this.searchBarValue).then((shows) => {
				this.shows = [ ...this.shows, ...shows];
				event.target.complete();
			});
		}
		else {
			this.apiService.getShows(this.currentPage).then((shows) => {
				this.shows = [ ...this.shows, ...shows];
				event.target.complete();
			});
		}
    event.target.disabled = this.shows.length === 1000;
  }

	likeOrUnlikeShow(showID: number, value: boolean) {
		var showToUpdate = this.shows.find((s) => s.id === showID);
		if (showToUpdate === null) return;
		
		showToUpdate.isLiked = value;

		if (value) {
			this.storageService.add(showToUpdate);
			this.notificationService.notification("Show added to favourite shows list.");
		}
		else {
			this.storageService.remove(showToUpdate.id);
			this.notificationService.notification("Show removed from favourite shows list.");
		}
	}

	showOrHideSearchbar() {
		this.currentPage = 1;
		this.showSearchbar = !this.showSearchbar;
		this.showSearchbar ? this.searchBarValue = "" : this.clearShows();
	}

	setSearchBarValue(event: any) {
		if (event.detail.value === undefined || event.detail.value === "") {
			this.searchBarValue = "";
			this.currentPage = 1;
		}
		else {
			this.searchBarValue = event.detail.value.toString();
		}
	}

	searchShows() {
		this.currentPage = 1;
		this.searchBarValue !== ""
			? this.apiService.searchShows(this.currentPage, this.searchBarValue).then((shows) => this.shows = shows)
			: this.clearShows();
	}

	clearShows() {
		this.currentPage = 1;
		this.getShows();
	}
}
