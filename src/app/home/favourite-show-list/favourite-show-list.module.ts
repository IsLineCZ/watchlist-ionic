import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { FavouriteShowListPageRoutingModule } from './favourite-show-list-routing.module';
import { FavouriteShowListPage } from './favourite-show-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FavouriteShowListPageRoutingModule
  ],
  declarations: [FavouriteShowListPage]
})
export class FavouriteShowListPageModule {}
