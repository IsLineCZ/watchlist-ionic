import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { ShowPreview } from 'src/app/models/show-preview.model';
import { NotificationService } from 'src/app/services/notification.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-favourite-show-list',
  templateUrl: './favourite-show-list.page.html',
  styleUrls: ['./favourite-show-list.page.scss'],
})
export class FavouriteShowListPage implements OnInit {
	shows: ShowPreview[] = [];

  constructor(
		private storageService: StorageService,
		private notificationService: NotificationService
	) {}

  ngOnInit() {}

	ionViewDidEnter() {
		this.loadFavouriteShows();
	}

	loadFavouriteShows() {
		this.shows = [];
		this.storageService.getAll().then((showIDs) => {
			showIDs.forEach(showID => {
				this.storageService.get(showID).then((show) => this.shows.push(show));
			});
		});
	}

	async unlikeShow(showID: number) {
		var show = this.shows.find((s) => s.id === showID);
		if (show === null) return;

		this.notificationService.confirmModal(
			show.name, 
			"Do you wish to remove this show from favourite shows list?", 
			"Remove", 
			() => {
				this.shows.splice(this.shows.indexOf(show), 1);
				this.storageService.remove(show.id);
				this.notificationService.notification("Show removed from favourite shows list.");
			}
		);
  }

	async unlikeAllShows() {
		this.notificationService.confirmModal(
			"Clear favourite shows list", 
			"Do you wish to remove all shows from favourite shows list?", 
			"Remove all",
			() => {
				this.shows.forEach((show) => this.storageService.remove(show.id))
				this.shows = [];
				this.notificationService.notification("All shows removed from favourite shows list.");
			}
		);
	}
}
