import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FavouriteShowListPage } from './favourite-show-list.page';

const routes: Routes = [
  {
    path: '',
    component: FavouriteShowListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavouriteShowListPageRoutingModule {}
