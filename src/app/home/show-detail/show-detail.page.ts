import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Show } from 'src/app/models/show.model';
import { ApiService } from 'src/app/services/api.service';
import { StorageService } from 'src/app/services/storage.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-show-detail',
  templateUrl: './show-detail.page.html',
  styleUrls: ['./show-detail.page.scss'],
})
export class ShowDetailPage implements OnInit {
	get imageUrl() {
		return this.show?.backdrop_path === null || this.show?.backdrop_path === undefined ? 
			"https://i.imgur.com/DyojSby.png" : 
			`https://image.tmdb.org/t/p/w500${this.show?.backdrop_path}`;
	}
	
	get genres() {
		if (this.show?.genres.length === 0) {
			return "Not specified";
		}
		else if (this.show?.genres.length === 1) {
			return this.show?.genres[0].name;
		}
		else {
			return this.show?.genres.map(genre => genre.name).join(", ");
		}
	}

	showID: number;
	show: Show;

  constructor(
		private apiService: ApiService, 
		private route: ActivatedRoute,
	 	private storageService: StorageService,
		private location: Location
	) {}

  ngOnInit() {
		this.showID = Number(this.route.snapshot.paramMap.get('id'));
		this.apiService.getShow(this.showID).subscribe((show) => { 
			this.show = show;
			this.storageService.get(show.id).then((show) => this.show.isLiked = show !== null);
		});
	}

	getFormatedDate(firstAirDate: Date | undefined): string {
		return firstAirDate === undefined ? "-" : moment(firstAirDate).format('DD.MM.YYYY');
	}

	goBack = () => this.location.back();
}
