import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{ path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule) },
	{ path: 'favourite-show-list', loadChildren: () => import('./home/favourite-show-list/favourite-show-list.module').then( m => m.FavouriteShowListPageModule) },
	{ path: 'show-detail/:id', loadChildren: () => import('./home/show-detail/show-detail.module').then( m => m.ShowDetailPageModule) },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
