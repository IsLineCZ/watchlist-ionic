import { ShowPreview } from "./show-preview.model";

export interface ApiResponse {
	page: number;
	results: ShowPreview[];
	total_pages: number;
	total_results: number;
}