export interface ShowPreview {
	id: number;
	name: string;
	isLiked: boolean;
}