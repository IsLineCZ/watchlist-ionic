import { Injectable } from '@angular/core';
import { ShowPreview } from '../models/show-preview.model';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})

export class StorageService {
  public async add(show: ShowPreview) {
		await Storage.set({
			key: show.id.toString(),
			value: JSON.stringify(show),
		});
  }

	public async get(showID: number): Promise<ShowPreview> {
		var show = await Storage.get({ key: showID.toString() });
		return JSON.parse(show.value);
  }

	public async remove(showID: number) {
		await Storage.remove({ key: showID.toString() });
  }

	public async getAll(): Promise<any> {
		return (await Storage.keys()).keys;
	}
}
