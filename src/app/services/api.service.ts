import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'; 
import { ApiResponse } from '../models/apiResponse.model';
import { Show } from '../models/show.model';
import { StorageService } from './storage.service';
import { ShowPreview } from '../models/show-preview.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
	private readonly baseUrl = 'https://api.themoviedb.org/3';

  constructor(
		private httpClient: HttpClient, 
		private storageService: StorageService
	) {}

	public async getShows(page: number = 1): Promise<ShowPreview[]> {
		var apiResponse = await this.httpClient.get<ApiResponse>(`${this.baseUrl}/discover/tv?page=${page}&${environment.apiKey}`).toPromise();
		return this.setShowsLikeStatus(apiResponse);
	}

	getShow(showID: number): Observable<Show> {
		return this.httpClient.get<Show>(`${this.baseUrl}/tv/${showID}?${environment.apiKey}`);
	}

	public async searchShows(page: number = 1, query: string): Promise<ShowPreview[]> {
		var apiResponse = await this.httpClient.get<ApiResponse>(`${this.baseUrl}/search/tv?${environment.apiKey}&page=${page}&query=${query}`).toPromise();
		return this.setShowsLikeStatus(apiResponse);
	}

	private setShowsLikeStatus(apiResponse: ApiResponse): ShowPreview[] {
		apiResponse.results.forEach((show) => { 
			this.storageService.get(show.id).then((s) => show.isLiked = s !== null);
		});

		return apiResponse.results;
	}
}
