import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
		private toastController: ToastController, 
		private alertController: AlertController
	) {}

	public async notification(text: string, duration: number = 1000) {
    const toast = await this.toastController.create({
      message: text,
      duration: duration
    });

    toast.present();
  }

	public async confirmModal(header: string, message: string, buttonText: string, fn: () => void) {
		const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        }, 
				{
          text: buttonText,
          handler: fn
        }
      ]
    });

    await alert.present();
	}
}
