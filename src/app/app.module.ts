import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ApiService } from './services/api.service';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage-angular';
import { StorageService } from './services/storage.service';

@NgModule({
  declarations: [
		AppComponent
	],
  entryComponents: [],
  imports: [
		BrowserModule,
		IonicModule.forRoot(),
		IonicStorageModule.forRoot(),
		AppRoutingModule,
		HttpClientModule
	],
  providers: [
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, 
		ApiService,
		StorageService
	],
  bootstrap: [
		AppComponent
	],
})
export class AppModule {}
